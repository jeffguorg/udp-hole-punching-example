import socket
import json
import time
import logging
import uuid

ip_mapping = {}
time_mapping = {}

try:
    sock = socket.socket(type=socket.SOCK_DGRAM)
    sock.bind(("", 1995))
    logging.basicConfig(level=logging.INFO)

    logging.info("listening at {}".format(sock.getsockname()))
    myid = "s:"+uuid.uuid1().hex
    while True:
        pack, addr = sock.recvfrom(1024)
        obj = json.loads(pack.decode())
        reply = {"_opid": obj['_opid'], "_myid": myid}
        logging.info("received request {} from {}".format(obj, addr))
        if obj['method'] == "register":
            # register an ip
            if obj['id'] in ip_mapping:
                reply.update({"result": "err", "reason": "conflict"})
            else:
                ip_mapping.update({obj["id"]: addr})
                time_mapping.update({obj['id']: time.time()})
                reply.update({"result": "ok"})
        elif obj['method'] == "query":
            result = []
            for host in ip_mapping.keys():
                if host.startswith("c:"+obj['id']):
                    result.append(host)

            if len(result) == 1:
                reply.update({
                    "result": "ok", 
                    "addr": ip_mapping[result[0]],
                    "status": "online"
                })
                if time.time() - time_mapping[host] > 10:
                    reply.update({
                        "status": "offline"
                    })
            elif len(result) > 1:
                reply.update({
                    "result": "err", 
                    "reason": "too many results",
                    "items": result
                })
            else:
                reply.update({
                    "result": "err",
                    "reason": "not found"
                })
        elif obj['method'] == "update":
            time_mapping.update({obj['id']: time.time()})
            ip_mapping.update({obj["id"]: addr})
            reply.update({"result": "ok", "addr": addr})
        elif obj['method'] == "list":
            reply.update({"result": "ok", "hosts": [name for name in ip_mapping.keys()]})
        else:
            reply.update({"result": "err", "reason": "unkownn command"})

        length = sock.sendto(json.dumps(reply).encode(), addr)
        logging.info("sending reply {} to {} ({})".format(reply, addr, length))
except KeyboardInterrupt:
    logging.info("quiting")
except Exception as e:
    logging.info("socket error")
    raise e
finally:
    logging.info("closing socket")
    sock.close()
