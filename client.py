import atexit
import socket
import logging
import uuid
import os
import sys
import json
import threading
import time
import queue
import re

import readline

# setup readline
try:
    readline.read_init_file(os.path.expanduser("~/.punchingrc"))
except:
    pass

try:
    readline.read_history_file(os.path.expanduser("~/.punching_history"))
except:
    pass

atexit.register(readline.write_history_file, os.path.expanduser("~/.punching_history"))

# 2to3 compatible
try:
    input = raw_input
except:
    pass

logging.basicConfig(level=logging.DEBUG, filename=os.path.expanduser("~/.punching_log"))
#logger.addHandler(logging.FileHandler(os.path.expanduser("~/.punching_log")))

# main part

try:    
    sock = socket.socket(type=socket.SOCK_DGRAM)
    sock.bind(("", 0))
    logging.info("client bound to {}".format(sock.getsockname()))
    print("client bound to {}".format(sock.getsockname()))

    try:
        with open(os.path.expanduser("~/.punching_id")) as fp:
            myid = fp.read()
        logging.info("get myid from last time: {}".format(myid))
    except:
        myid = "c:"+uuid.uuid1().hex
        logging.info("generated a new myid: {}".format(myid))
        sock.sendto(json.dumps({"method": "register", "id": myid, "_opid": myid}).encode(), (sys.argv[1], int(sys.argv[2])))

        data, addr = sock.recvfrom(1024)
        obj = json.loads(data.decode())
        while obj['_opid'] != myid:
            data, addr = sock.recvfrom(1024)
            obj = json.loads(data.decode())

        if obj['result'] == "ok":
            logging.debug("myid registered")
        elif obj['result'] == "err":
            logging.debug("failed to register myid: {}".format(obj['reason']))
            sys.exit(-1)
        else:
            logging.debug("failed to parsing reply when registering myid: {}".format(obj['reason']))
            sys.exit(-1)

        logging.info("saving myid to local")
        with open(os.path.expanduser("~/.punching_id"), "w+") as fp:
            fp.write(myid)

    hosts = set()
    q = queue.Queue()

    network_thread_result = None
    def network():
        last_time_update = 0
        last_time_reply = time.time()

        operations = dict()

        quit = False
        while not quit:
            opid = uuid.uuid1().hex
            request = {"_opid": opid, "_myid": myid}
            if time.time() - last_time_update > 3:
                request.update({"method": "update", "id": myid})
                operations.update({opid: {"op": "update"}})
                sock.sendto(json.dumps(request).encode(), (sys.argv[1], int(sys.argv[2])))
                last_time_update = time.time()
                logging.debug("sending request {} to {}".format(request, (sys.argv[1], int(sys.argv[2]))))
            while not q.empty():
                obj = q.get(block=False)
                if obj['operation'] == "exit":
                    network_thread_result = 0
                    return
                else:
                    if obj['operation'] == "list":
                        request.update({"method": "list"})
                        operations.update({opid: {"op": "list"}})
                    elif obj['operation'] == "send":
                        request.update({"method": "query", "id": obj['id']})
                        operations.update({opid: {"op": "send", "id": obj['id'], "msg": obj['msg']}})
                    else:
                        logging.error("unknown operation: {}".format(obj['operation']))
                if 'method' in request:
                    sock.sendto(json.dumps(request).encode(), (sys.argv[1], int(sys.argv[2])))

            try:
                sock.setblocking(False)
                while True:
                    data, addr = sock.recvfrom(1024)
                    logging.debug("received {} from {}".format(data, addr))
                    
                    obj = json.loads(data.decode())
                    logging.debug(obj)
                    if obj['_myid'].startswith("s:"):
                        if '_opid' in obj:
                            op = operations[obj['_opid']]['op']
                            if op == "send":
                                if obj['result'] == "ok":
                                    if 'addr' in obj:
                                        request.update({"method": "text", "load": operations[obj['_opid']]['msg']})
                                        addr = tuple(obj['addr'])
                                        sock.sendto(json.dumps(request).encode(), addr)
                                        logging.debug("sending request {} to {}".format(request, addr))
                                        print("sending request {} to {}".format(request, addr))
                                else:
                                    print("\r!! error: {}".format(obj['reason']))
                                    if obj['reason'] == "too many results":
                                        for item in obj['items']:
                                            print("\r!!   > {}".format(item))
                            elif op == "list":
                                if obj['result'] == "ok":
                                    print("\r<< get list:")
                                    for item in obj['hosts']:
                                        print("\r<< - {}".format(item))
                                else:
                                    print("\r!! error: {}".format(obj['reason']))
                    else:
                        if 'method' in obj:
                            if obj['method'] == "text":
                                print("\r<< {}".format(obj['load']))
                    print("\r>> ", end="")


            except BlockingIOError as e:
                pass
            finally:
                sock.setblocking(True)

    network_thread = threading.Thread(target=network)
    network_thread.start()
    logging.info("network thread started")
    print("network thread started")

    while True:
        print(">> ", end="")
        line = input()
        if re.fullmatch("\s*list\s*", line) is not None:
            q.put({"operation": "list"})
        elif re.fullmatch("\s*send\s*>\s*(\w+)\s*:\s*(.*)\s*", line) is not None:
            dest, msg = re.fullmatch("\s*send\s*>\s*(\w+)\s*:\s*(.*)\s*", line).group(1,2)
            q.put({"operation": "send", "id": dest, "msg": msg})
except KeyboardInterrupt:
    logging.info("exiting due to keyboard interrupt")
except Exception as e:
    logging.error("something wrong: {}".format(e))
finally:
    q.put({"operation": "exit"})
    network_thread.join()
    logging.info("network thread exit with {}".format(network_thread_result))
    
    
    logging.info("closing socket")
    sock.close()
